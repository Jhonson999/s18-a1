console.log("Hello World!")
//Create a "trainer" object by using object literals
let trainer ={
	    name: "Gyver",
        age: 26,
        birthday: "January 21,1995",
        gender: "male",
        proficiency: "intermediate",
        summon: function(){
        	console.log("I choose you!")
        }
};
//dot notation
console.log(trainer.name);
//bracket notation
console.log(trainer['pokemon']);
//Invoke/call the "trainer" object method.
console.log(trainer.summon());

//Create a constructor function for creating pokemon.
function Pokemon(name, health, attack, defense){
	this.name = name;
	this.health = health;
	this.attack = attack;
	this.defense = defense;
	this. tackle = function(target){
          console.log(`${this.name} attacked ${target.name}`);
          target. health -=50;
          console.log(`${this.name}'s health is now reduced to ${target.health}'`);
	}

};

let Charmander = new Pokemon("Charmander", 150, 200, 100);
console.log(Charmander);

let Balbasaur = new Pokemon("Balbasaur", 140, 200, 120);
console.log(Balbasaur);

Charmander.tackle(Balbasaur);




